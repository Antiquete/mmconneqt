// Copyright (C) 2019 Hari Saksena <antiquete@proton.me>
// 
// This file is part of MMconneqt.
// 
// MMconneqt is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// MMconneqt is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with MMconneqt.  If not, see <http://www.gnu.org/licenses/>.

#include "propui.h"

void Propui::hide(){
    this->setEnabled(false);
}
QString Propui::parseInt(int val)
{
    return QString::number(val);
}
QString Propui::parseString(QString val)
{
    return val != "" ? val : "None";
}
QString Propui::parseStringList(QStringList val)
{
    return val.count() > 0 ? "[" + val.join(",") + "]" : "None";
}
QString Propui::parseBoolean(bool val)
{
    return val ? "Yes" : "No";
}
QString Propui::parseD_SV(D_SV val)
{
    QStringList entries;
    D_SV::const_iterator i  = val.begin();
    while(i != val.end())
    {
        entries.push_back("{" + i.key() + ":" + parse(i.value()) + "}");
        ++i;
    }
    return parseStringList(entries);
}
QString Propui::demarshall(QDBusArgument arg)
{
    QString t = arg.currentSignature();
    QString value = "None";

    // Enum for switch case
    enum {
        s_uu,
        s_ub,
        ao,
        au,
        a_s_su,
        a_s_uu,
        a_s_uba_y,
        d_uu,
        d_uv,
        d_sv,
        s_ud_sv
    };

    switch (_types.indexOf(t))
    {
    case s_uu:
        return demarshall_s_uu(arg);
        break;

    case s_ub:
        return demarshall_s_ub(arg);
        break;

    case ao:
        return demarshall_ao(arg);
        break;

    case au:
        return demarshall_au(arg);
        break;

    case a_s_su:
        return demarshall_a_s_su(arg);
        break;

    case a_s_uu:
        return demarshall_a_s_uu(arg);
        break;

    case a_s_uba_y:
        return demarshall_a_s_uba_y(arg);
        break;

    case d_uu:
        return demarshall_d_uu(arg);
        break;

    case d_uv:
        return demarshall_d_uu(arg);
        break;

    case d_sv:
        return parseD_SV(arg.asVariant().value<D_SV>());
        break;

    case s_ud_sv:
        return demarshall_s_ud_sv(arg);
        break;

    default:
        qDebug().noquote().nospace()
            << "Demarshall (" <<  this->property("TargetMt").toInt() << ":" << this->property("Target").toString() << "): Type not handled: " << t;
        return "Not Found";
    }
}
QString Propui::demarshall_s_uu(const QDBusArgument &arg)
{
    uint u1, u2;
    arg.beginStructure();
    arg >> u1 >> u2;
    arg.endStructure();
    return "{" + parseInt(u1) + "," + parseInt(u2) + "}";
}
QString Propui::demarshall_s_ub(const QDBusArgument &arg)
{
    uint u;
    bool b;
    arg.beginStructure();
    arg >> u >> b;
    arg.endStructure();
    return "{" + parseInt(u) + "," + parseBoolean(b) + "}";
}
QString Propui::demarshall_s_su(const QDBusArgument &arg)
{
    QString s;
    uint u;
    arg.beginStructure();
    arg >> s >> u;
    arg.endStructure();
    return "{" + s + "," + parseInt(u) + "}";
}
QString Propui::demarshall_ao(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginArray();
    while (!arg.atEnd())
    {
        QDBusObjectPath op;
        arg >> op;
        entries.push_back(op.path());
    }
    arg.endArray();
    return parseStringList(entries);
}
QString Propui::demarshall_au(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginArray();
    while (!arg.atEnd())
    {
        uint u;
        arg >> u;
        entries.append(parseInt(u));
    }
    arg.endArray();
    return parseStringList(entries);
}
QString Propui::demarshall_a_s_su(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginArray();
    while (!arg.atEnd())
    {
        entries.push_back(demarshall_s_su(arg));
    }
    arg.endArray();
    return parseStringList(entries);
}
QString Propui::demarshall_a_s_uu(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginArray();
    while (!arg.atEnd())
    {
        entries.push_back(demarshall_s_uu(arg));
    }
    arg.endArray();
    return parseStringList(entries);
}
QString Propui::demarshall_a_s_uba_y(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginArray();
    while (!arg.atEnd())
    {
        uint u;
        bool b;
        QVariant ayv;
        arg.beginStructure();
        arg >> u >> b >> ayv;
        QDBusArgument ayarg = ayv.value<QDBusArgument>();
        QStringList ay;
        ayarg.beginArray();
        while (!ayarg.atEnd())
        {
            uint8_t y;
            ayarg >> y;
            ay.push_back(parseInt(y));
        }
        ayarg.endArray();
        arg.endStructure();
        entries.push_back(parseInt(u) + "," + parseBoolean(b) + "," + parseStringList(ay));
    }
    arg.endArray();
    return parseStringList(entries);
}
QString Propui::demarshall_d_uu(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginMap();
    while (!arg.atEnd())
    {
        uint u1, u2;
        arg.beginMapEntry();
        arg >> u1 >> u2;
        arg.endMapEntry();
        entries.push_back(parseInt(u1) + ":" + parseInt(u2));
    }
    arg.endMap();
    return entries.count() > 0 ? "{" + entries.join(",") + "}" : "None";
}
QString Propui::demarshall_d_uv(const QDBusArgument &arg)
{
    QStringList entries;
    arg.beginMap();
    while (!arg.atEnd())
    {
        uint u;
        QVariant v;
        arg.beginMapEntry();
        arg >> u >> v;
        arg.endMapEntry();
        entries.push_back(parseInt(u) + ":" + parse(v));
    }
    arg.endMap();
    return entries.count() > 0 ? "{" + entries.join(",") + "}" : "None";
}
QString Propui::demarshall_s_ud_sv(const QDBusArgument &arg)
{
    QStringList entries;
    uint u;
    D_SV d_sv;
    arg.beginStructure();
    arg >> u >> d_sv;
    entries.push_back("{" + parseInt(u) + "," + parseD_SV(d_sv) + "}");
    arg.endStructure();
    return parseStringList(entries);
}

QString Propui::parse(QVariant variant)
{
    switch (variant.userType())
    {
    case QMetaType::Int:
    case QMetaType::UInt:
    case QMetaType::LongLong:
    case QMetaType::ULongLong:
        return parseInt(variant.value<int>());

    case QMetaType::QString:
        return parseString(variant.value<QString>());

    case QMetaType::QStringList:
        return parseStringList(variant.value<QStringList>());

    case QMetaType::Bool:
        return parseBoolean(variant.value<bool>());

    case 1028:
    case 1031:  // Object Path
        return parseString(variant.value<QDBusObjectPath>().path());

    case 1026:
    case 1029: // DBusArgument (Demarshall)
        return demarshall(variant.value<QDBusArgument>());

    default:
        qDebug() << "Not Implemented: " << this->property("Target") << "-> Type:" << variant.userType();
        return "Not Found";
    }
}

void Propui::set(QVariant variant)
{
    QString value = parse(variant);
    this->setText(value);
    if(value == "None" || value == "Not Found")
        this->hide();
}
