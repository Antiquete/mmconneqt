// Copyright (C) 2019 Hari Saksena <antiquete@proton.me>
// 
// This file is part of MMconneqt.
// 
// MMconneqt is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// MMconneqt is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with MMconneqt.  If not, see <http://www.gnu.org/licenses/>.

#ifndef PROPUI_H
#define PROPUI_H

#include <QLabel>
#include <QGroupBox>
#include <QDBusArgument>

#include "core.h"

class Propui : public QLabel
{
    Q_OBJECT
private:
    QStringList _types = {
                          "(uu)",
                          "(ub)",
                          "ao",
                          "au",
                          "a(su)",
                          "a(uu)",
                          "a(ubay)",
                          "a{uu}",
                          "a{uv}",
                          "a{sv}",
                          "(ua{sv})"
                         };

    void hide();
    QString parseInt(int val);
    QString parseString(QString val);
    QString parseStringList(QStringList val);
    QString parseBoolean(bool val);
    QString parseD_SV(D_SV val);
    QString demarshall(QDBusArgument arg);
    QString parse(QVariant variant);

    QString demarshall_s_uu(const QDBusArgument &arg);
    QString demarshall_s_ub(const QDBusArgument &arg);
    QString demarshall_s_su(const QDBusArgument &arg);
    QString demarshall_ao(const QDBusArgument &arg);
    QString demarshall_au(const QDBusArgument &arg);
    QString demarshall_a_s_su(const QDBusArgument &arg);
    QString demarshall_a_s_uu(const QDBusArgument &arg);
    QString demarshall_a_s_uba_y(const QDBusArgument &arg);
    QString demarshall_d_uu(const QDBusArgument &arg);
    QString demarshall_d_uv(const QDBusArgument &arg);
    QString demarshall_s_ud_sv(const QDBusArgument &arg);

public:
    Propui(QWidget *parent = nullptr) {}

public slots:
    void set(QVariant variant);
};

#endif // PROPUI_H
